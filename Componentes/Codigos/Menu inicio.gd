extends Control

@export var inicio: Panel
@export var jugadores: Panel
@export var mapas: Panel
@export_file('*.tscn') var mapaPrueba
var controlesTexto = [1, 2, 3]
var controlesTextoIndise: int
@export var instruccionesTexto: PackedStringArray
var instruccionesIndise: int

func _ready():
	jugadores.visible = false
	mapas.visible = false
	GLOBAL.dosJugadores = false
	GLOBAL.tresJugadores = false
	GLOBAL.cuatroJugadores = false
	$Musica.play()
	get_window().content_scale_mode = 1
	controlesTexto[0] = "Jugador 1\navanzar, retroceder: w, s\ndoblar: a, d\nsaltar: espacio\ndisparar: control"
	controlesTexto[1] = "Jugador 2, 3, 4 (joystick)\navanzar, retroceder: arriba, abajo\ndoblar: lb, rb\nsaltar: a\ndisparar: x"
	controlesTexto[2] = "pausa: escape"
	$Controles.visible = false
	$Instucciones.visible = false
	$Creditos.visible = false

func _process(delta):
	$Controles/Texto.text = controlesTexto[controlesTextoIndise]
	$Instucciones/Texto.text = instruccionesTexto[instruccionesIndise]

func _on_jugar_pressed():
	$Aseptar.play()
	jugadores.visible = true
	inicio.visible = false

func _on_volver_al_inicio_pressed():
	$Canselar.play()
	inicio.visible = true
	jugadores.visible = false

func _on_dos_jugadores_pressed():
	$Aseptar.play()
	GLOBAL.dosJugadores = true
	mapas.visible = true
	jugadores.visible = false

func _on_tres_jugadores_pressed():
	$Aseptar.play()
	GLOBAL.tresJugadores = true
	mapas.visible = true
	jugadores.visible = false

func _on_cuatro_jugadores_pressed():
	$Aseptar.play()
	GLOBAL.cuatroJugadores = true
	mapas.visible = true
	jugadores.visible = false

func _on_salir_pressed():
	$Canselar.play()
	get_tree().quit()

func _on_volver_a_jugador_pressed():
	$Canselar.play()
	jugadores.visible = true
	GLOBAL.dosJugadores = false
	GLOBAL.tresJugadores = false
	GLOBAL.cuatroJugadores = false
	mapas.visible = false

func _input(event):
	if Input.is_key_pressed(KEY_P) && mapas.visible:
		get_tree().change_scene_to_file(mapaPrueba)

func _on_mapa_1_pressed():
	$Aseptar.play()
	Transicion.activar("res://Componentes/Esenas/mapa_1.tscn")

func _on_mapa_2_pressed():
	$Aseptar.play()
	Transicion.activar("res://Componentes/Esenas/mapa_2.tscn")

func _on_derecha_controles_pressed():
	if controlesTextoIndise + 1 != controlesTexto.size():
		$Aseptar.play()
		controlesTextoIndise += 1

func _on_izquierda_controles_pressed():
	if controlesTextoIndise != 0:
		$Aseptar.play()
		controlesTextoIndise -= 1


func _on_controles_pressed():
	$Aseptar.play()
	$Controles.visible = true
	$Inicio.visible = false


func _on_volver_controles_pressed():
	$Canselar.play()
	$Inicio.visible = true
	$Controles.visible = false
	controlesTextoIndise = 0


func _on_instrucciones_derecha_pressed():
	if instruccionesIndise + 1 != instruccionesTexto.size():
		$Aseptar.play()
		instruccionesIndise += 1

func _on_instrucciones_izquierda_pressed():
	if instruccionesIndise != 0:
		$Aseptar.play()
		instruccionesIndise -= 1


func _on_volver_de_instrucciones_pressed():
	$Canselar.play()
	$Inicio.visible = true
	$Instucciones.visible = false
	instruccionesIndise = 0


func _on_intrucciones_pressed():
	$Aseptar.play()
	$Instucciones.visible = true
	$Inicio.visible = false


func _on_creditos_pressed():
	$Aseptar.play()
	$Creditos.visible = true
	$Inicio.visible = false


func _on_volver_de_creditos_pressed():
	$Canselar.play()
	$Inicio.visible = true
	$Creditos.visible = false


func _on_mapa_3_pressed():
	$Aseptar.play()
	Transicion.activar("res://Componentes/Esenas/mapa_3.tscn")
