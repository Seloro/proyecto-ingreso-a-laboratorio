extends RigidBody3D

var x: int
var y: int
var z: int
var signo: bool

func _ready():
	elecciondeMovimioento()
	$Timer.start(5)

func _process(delta):
	if !GLOBAL.fin:
		constant_force = Vector3(x, y, z) * 25000
	else:
		constant_force = Vector3.ZERO
		linear_velocity = Vector3.ZERO
		angular_velocity = Vector3.ZERO


func _on_timer_timeout():
	if !GLOBAL.fin:
		elecciondeMovimioento()
		linear_velocity = Vector3.ZERO
		$Timer.start(5)

func elecciondeMovimioento():
	signo = randi_range(0, 1)
	if signo:
		x = randi_range(0, 1)
	else:
		x = randi_range(0, 1) * -1
	signo = randi_range(0, 1)
	if signo:
		y = randi_range(0, 1)
	else:
		y = randi_range(0, 1) * -1
	signo = randi_range(0, 1)
	if signo:
		z = randi_range(0, 1)
	else:
		z = randi_range(0, 1) * -1
