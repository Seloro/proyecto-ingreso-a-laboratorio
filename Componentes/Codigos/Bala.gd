extends Area3D

var temp: float
var signo: int

func _ready():
	if get_parent().name == "Torres":
		signo = 1
	else:
		signo = -1

func _process(delta):
	pass
	if temp > 5:
		queue_free()
	temp += delta
	global_position += (global_basis * Vector3.FORWARD * signo).normalized() * 2

func _on_body_entered(body):
	pass
	if get_parent() != body.get_parent():
		queue_free()

func _on_area_entered(area):
	if area.get_parent().name == "Cargadores" || area.get_parent().name == "Vidas" || area.get_parent().name == "Meta":
		queue_free()
