extends Panel

@export_file('*.tscn') var menuInicio

func _ready():
	visible = false

func _process(delta):
	if GLOBAL.pausa:
		visible = true
	else:
		visible = false

func _on_salir_pressed():
	$"../Aseptar".play()
	get_tree().paused = false
	GLOBAL.pausa = false


func _on_reiniciar_pressed():
	$"../Aseptar".play()
	get_tree().paused = false
	GLOBAL.pausa = false
	Transicion.activar(GLOBAL.nombreDeEsena)


func _on_salir_al_menu_pressed():
	$"../Aseptar".play()
	get_window().content_scale_mode = 0
	get_tree().paused = false
	GLOBAL.pausa = false
	Transicion.activar("res://Componentes/Esenas/menu_inicio.tscn")
