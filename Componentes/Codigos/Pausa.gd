extends Node

func _ready():
	GLOBAL.pausa = false

func _unhandled_key_input(event):
	if Input.is_key_label_pressed(KEY_ESCAPE):
		get_tree().paused = !get_tree().paused
		GLOBAL.pausa = get_tree().paused
