extends Control

@export var jugadorCuatro: PackedScene

func _ready():
	$Gana.visible = false
	if GLOBAL.cuatroJugadores:
		$"Jugadores/HBoxContainer2/Sin jugador 4".visible = false
		$"Jugadores/HBoxContainer2/Jugador 4/Jugador 4".add_child(jugadorCuatro.instantiate())
	else:
		$"Jugadores/HBoxContainer2/Jugador 4".visible = false

func _process(delta):
	if GLOBAL.fin:
		if !$Gana.visible:
			if max(GLOBAL.puntosJugadorUno, GLOBAL.puntosJugadorDos, GLOBAL.puntosJugadorTres, GLOBAL.puntosJugadorCuatro) == GLOBAL.puntosJugadorUno:
				$Gana/Jugador.text = "Jugador 1"
			elif  max(GLOBAL.puntosJugadorUno, GLOBAL.puntosJugadorDos, GLOBAL.puntosJugadorTres, GLOBAL.puntosJugadorCuatro) == GLOBAL.puntosJugadorDos:
				$Gana/Jugador.text = "Jugador 2"
			elif max(GLOBAL.puntosJugadorUno, GLOBAL.puntosJugadorDos, GLOBAL.puntosJugadorTres, GLOBAL.puntosJugadorCuatro) == GLOBAL.puntosJugadorTres:
				$Gana/Jugador.text = "Jugador 3"
			elif max(GLOBAL.puntosJugadorUno, GLOBAL.puntosJugadorDos, GLOBAL.puntosJugadorTres, GLOBAL.puntosJugadorCuatro) == GLOBAL.puntosJugadorCuatro:
				$Gana/Jugador.text = "Jugador 4"
			else:
				$Gana/Ganador.visible = false
				$Gana/Jugador.text = "Empate"
		$Gana.visible = true
	if GLOBAL.inicio:
		if GLOBAL.tempInicio <= 3:
			$Inico.text = str(int(GLOBAL.tempInicio) + 1)
	else:
		$Inico.visible = false


func _on_volver_pressed():
	$Aseptar.play()
	Transicion.activar("res://Componentes/Esenas/menu_inicio.tscn")

func _on_reintentar_pressed():
	$Aseptar.play()
	Transicion.activar(GLOBAL.nombreDeEsena)
