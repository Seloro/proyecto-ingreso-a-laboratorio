extends Area3D

var y: float
var cambio: bool

func _ready():
	y = global_position.y
	cambio = randi_range(0, 2)
	$Timer.start(GLOBAL.metaTemp)

func _process(delta):
	rotate_y(delta)
	if global_position.y >= y + 0.1:
		cambio = false
	elif global_position.y <= y -0.1:
		cambio = true
	if cambio:
		global_position.y += delta / 2
	else:
		global_position.y -= delta / 2

func _on_area_entered(area):
	GLOBAL.metaEsta = false
	queue_free()

func _on_body_entered(body):
	if body.name != "Jugador":
		GLOBAL.metaEsta = false
		queue_free()

func _on_timer_timeout():
	GLOBAL.metaEsta = false
	queue_free()
