extends VehicleBody3D

var balas = preload("res://Componentes/Prefabricados/bala.tscn")
@export var salto: RayCast3D
@export var miraUno: Marker3D
@export var miraDos: Marker3D
@export var balasBarra: ProgressBar
@export var vidaBarra: ProgressBar
@export var nombreEtiqueta: Label
@export var puntosEtiqueta: Label
@export var temporizadorEtiqueta: Label
@export var comprobacionUno: RayCast3D
@export var comprobacionDos: RayCast3D
const aceleracio:= 400
var balasActuales: float
var vidaActual: float
var balsTemp: float
var tempReposicionamiento: float
#<>

func _ready():
	nombreEtiqueta.text = "Jugador 1"
	vidaActual = vidaBarra.max_value
	GLOBAL.puntosJugadorUno = 0
	position = GLOBAL.posicionUno
	rotation = GLOBAL.rotacionUno
	$"Camara/Interfas de jugador/Indicador".visible = false

func _process(delta):
	vidaBarra.value = vidaActual
	balasBarra.value = balasActuales
	if !GLOBAL.metaEsta:
		$"Camara/Interfas de jugador/Indicador".visible = true
	if !GLOBAL.temp.is_stopped() && !GLOBAL.fin && !GLOBAL.inicio:
		puntosEtiqueta.text = "Puntos: " + var_to_str(GLOBAL.puntosJugadorUno)
		temporizadorEtiqueta.text = "Tiempo: " + "%ds" %GLOBAL.temp.time_left
		reinicio(delta)
		controles(delta)
	else:
		brake = 0
		engine_force = 0
		linear_velocity = Vector3.ZERO

func controles(delta):
	if Input.is_key_pressed(KEY_W):
		if engine_force < 0:
			brake = 600
			engine_force = 0
		elif engine_force <= 1200:
			brake = 0
			engine_force += aceleracio * delta
	elif Input.is_key_pressed(KEY_S):
		if engine_force > 0:
			brake = 600
			engine_force = 0
		elif  engine_force >= -1200:
			brake = 0
			engine_force -= aceleracio * delta
	else:
		if engine_force > 1:
			engine_force -= aceleracio * delta
		elif  engine_force < -1:
			engine_force += aceleracio * delta
		else:
			engine_force = 0
	if Input.is_key_pressed(KEY_A):
		steering = 0.15
	elif  Input.is_key_pressed(KEY_D):
		steering = -0.15
	else:
		steering = 0
	if Input.is_key_label_pressed(KEY_SPACE):
		if salto.is_colliding():
			$Sonidos/Salto.play()
			linear_velocity.y += 4.5
	if balsTemp < .5:
		balsTemp += delta
	elif Input.is_key_label_pressed(KEY_CTRL) && balasActuales != 0:
		var balaUno = balas.instantiate()
		var balaDos = balas.instantiate()
		balaUno.position = miraUno.global_position
		balaUno.rotation = miraUno.global_rotation
		balaDos.position = miraDos.global_position
		balaDos.rotation = miraDos.global_rotation
		get_parent().add_child(balaUno)
		get_parent().add_child(balaDos)
		$Sonidos/Tiro.play()
		balasActuales -= 12.5
		balsTemp = 0

func reinicio(delta):
	if !comprobacionUno.is_colliding() && !salto.is_colliding() && !comprobacionDos.is_colliding():
		if tempReposicionamiento < 10:
			tempReposicionamiento += delta
		else:
			tempReposicionamiento = 0
			position = GLOBAL.posicionUno
			rotation = GLOBAL.rotacionUno
			$Sonidos/Reinicio.play()
	else:
		tempReposicionamiento = 0
	if vidaActual <= 0:
		vidaActual = vidaBarra.max_value
		position = GLOBAL.posicionUno
		rotation = GLOBAL.rotacionUno
		$Sonidos/Reinicio.play()

func _on_señales_area_entered(area):
	if area.get_parent().name == "Cargadores":
		$Sonidos/Carga.play()
		balasActuales += 25
		GLOBAL.puntosJugadorUno += 2
		if balasActuales >= balasBarra.max_value:
			balasActuales = balasBarra.max_value
	if area.get_parent().name == "Vidas":
		$Sonidos/Vida.play()
		vidaActual += 6.25
		GLOBAL.puntosJugadorUno += 1
		if vidaActual >= vidaBarra.max_value:
			vidaActual = vidaBarra.max_value
	if area.get_parent().name == "Jugador 2" || area.get_parent().name == "Jugador 3" || area.get_parent().name == "Jugador 4":
		$"Sonidos/Daño".play()
		vidaActual -= 12.5
		if vidaActual <= 0:
			if area.get_parent().name == "Jugador 2":
				GLOBAL.puntosJugadorDos += 50
			elif area.get_parent().name == "Jugador 3":
				GLOBAL.puntosJugadorTres += 50
			elif  area.get_parent().name == "Jugador 4":
				GLOBAL.puntosJugadorCuatro += 50
	if area.name == "Meta":
		$Sonidos/Meta.play()
		GLOBAL.fin = true
		GLOBAL.temp.stop()
		GLOBAL.puntosJugadorUno += 9999
	if area.get_parent().name == "Torres":
		$"Sonidos/Daño".play()
		vidaActual -= 6.25
