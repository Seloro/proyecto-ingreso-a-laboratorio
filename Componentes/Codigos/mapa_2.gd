extends Node3D

@export var cargadorEsena: PackedScene
@export var cargadoresMax: int
@export var vidaEsena: PackedScene
@export var vidasMax: int
@export var tiempoMaxEnS: int
@export var pantallaDosjugadores: PackedScene
@export var metaEsena: PackedScene
@export var metaTemp: int
@export var pantallaTresYCuatroJugadores: PackedScene

func _ready():
	GLOBAL.posicionUno = $"Bases/Base azul/Posision 1".global_position
	GLOBAL.rotacionUno = $"Bases/Base azul/Posision 1".global_rotation
	GLOBAL.posicionDos = $"Bases/Base roja/Posicion 2".global_position
	GLOBAL.rotacionDos = $"Bases/Base roja/Posicion 2".global_rotation
	GLOBAL.posicionTres = $"Bases/Base verde/Posicion 3".global_position
	GLOBAL.rotacionTres = $"Bases/Base verde/Posicion 3".global_rotation
	GLOBAL.posicionCuatro = $"Bases/Base amarilla/Posision 4".global_position
	GLOBAL.rotacionCuatro = $"Bases/Base amarilla/Posision 4".global_rotation
	$Musica.play()
	if GLOBAL.dosJugadores:
		add_child(pantallaDosjugadores.instantiate())
	elif  GLOBAL.tresJugadores || GLOBAL.cuatroJugadores:
		add_child(pantallaTresYCuatroJugadores.instantiate())
	GLOBAL.cargadoresMapa = 0
	GLOBAL.vidasMapa = 0
	GLOBAL.temp = Timer.new()
	GLOBAL.temp.one_shot = true
	add_child(GLOBAL.temp)
	GLOBAL.metaEsta = true
	GLOBAL.fin = false
	GLOBAL.metaTemp = metaTemp
	GLOBAL.inicio = true

func _process(delta):
	GLOBAL.tempInicio = $"Cuenta de inicio".time_left
	if int($"Cuenta de inicio".time_left) == 3:
		$"Cuenta de inicio/Tres".play()
	if int($"Cuenta de inicio".time_left) == 2:
		$"Cuenta de inicio/Dos".play()
	if int($"Cuenta de inicio".time_left) == 1:
		$"Cuenta de inicio/Uno".play()
	if GLOBAL.cargadoresMapa < cargadoresMax:
		var cargador = cargadorEsena.instantiate()
		cargador.position = Vector3(randf_range(-200,201), .65, randf_range(-200,201))
		$Cargadores.add_child(cargador)
		GLOBAL.cargadoresMapa += 1
	if GLOBAL.vidasMapa < vidasMax:
		var vidas = vidaEsena.instantiate()
		vidas.position = Vector3(randf_range(-200,201), .65, randf_range(-200,201))
		$Vidas.add_child(vidas)
		GLOBAL.vidasMapa += 1
	if !GLOBAL.metaEsta && !GLOBAL.fin:
		var meta = metaEsena.instantiate()
		meta.position = Vector3(randf_range(-100, 101), .65, randf_range(-100, 101))
		$Meta.add_child(meta)
		GLOBAL.metaEsta = true
	if GLOBAL.temp.is_stopped() && !GLOBAL.inicio:
		GLOBAL.fin = true

func _on_aparicion_meta_timeout():
	$"Aparicion meta/Sonido".play()
	GLOBAL.metaEsta = false

func _on_cuenta_de_inicio_timeout():
	$"Cuenta de inicio/Ya".play()
	GLOBAL.inicio = false
	GLOBAL.temp.start(tiempoMaxEnS)
	$"Aparicion meta".start(120)
