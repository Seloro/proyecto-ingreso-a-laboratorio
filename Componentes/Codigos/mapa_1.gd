extends Node3D

@export var cargadorEsena: PackedScene
@export var cargadoresMax: int
@export var vidaEsena: PackedScene
@export var vidasMax: int
@export var tiempoMaxEnS: int
@export var pantallaDosjugadores: PackedScene
@export var metaEsena: PackedScene
@export var caminoAlto: PathFollow3D
@export var conoEsena: PackedScene
@export var bloqueoEsena: PackedScene
@export var conosMax: int
@export var bloqueosMax: int
@export var metaTemp: int
@export var pantallaTresYCuatroJugadores: PackedScene

func _ready():
	GLOBAL.posicionUno = $"Bases/Base azul/Posision 1".global_position
	GLOBAL.rotacionUno = $"Bases/Base azul/Posision 1".global_rotation
	GLOBAL.posicionDos = $"Bases/Base roja/Posicion 2".global_position
	GLOBAL.rotacionDos = $"Bases/Base roja/Posicion 2".global_rotation
	GLOBAL.posicionTres = $"Bases/Base verde/Posicion 3".global_position
	GLOBAL.rotacionTres = $"Bases/Base verde/Posicion 3".global_rotation
	GLOBAL.posicionCuatro = $"Bases/Base amarilla/Posision 4".global_position
	GLOBAL.rotacionCuatro = $"Bases/Base amarilla/Posision 4".global_rotation
	$Musica.play()
	if GLOBAL.dosJugadores:
		add_child(pantallaDosjugadores.instantiate())
	elif  GLOBAL.tresJugadores || GLOBAL.cuatroJugadores:
		add_child(pantallaTresYCuatroJugadores.instantiate())
	GLOBAL.cargadoresMapa = 0
	GLOBAL.vidasMapa = 0
	GLOBAL.temp = Timer.new()
	GLOBAL.temp.one_shot = true
	add_child(GLOBAL.temp)
	GLOBAL.metaEsta = true
	GLOBAL.fin = false
	GLOBAL.metaTemp = metaTemp
	GLOBAL.inicio = true

func _process(delta):
	GLOBAL.tempInicio = $"Cuenta de inicio".time_left
	if int($"Cuenta de inicio".time_left) == 3:
		$"Cuenta de inicio/Tres".play()
	if int($"Cuenta de inicio".time_left) == 2:
		$"Cuenta de inicio/Dos".play()
	if int($"Cuenta de inicio".time_left) == 1:
		$"Cuenta de inicio/Uno".play()
	if GLOBAL.cargadoresMapa < cargadoresMax:
		var indise: int
		var cargador = cargadorEsena.instantiate()
		indise = randi_range(0, 11)
		if indise <= 6:
			cargador.position = Vector3(randf_range(-200,201), .65, randf_range(-200,201))
		else:
			caminoAlto.progress_ratio = randf()
			cargador.position = caminoAlto.global_position + Vector3(randf_range(-5, 6), 0, randf_range(-15, 16))
		$Cargadores.add_child(cargador)
		GLOBAL.cargadoresMapa += 1
	if GLOBAL.vidasMapa < vidasMax:
		var indise: int
		var vidas = vidaEsena.instantiate()
		indise = randi_range(0, 11)
		if indise <= 7:
			vidas.position = Vector3(randf_range(-200,201), .65, randf_range(-200,201))
		else:
			caminoAlto.progress_ratio = randf()
			vidas.position = caminoAlto.global_position + Vector3(randf_range(-5, 6), 0, randf_range(-15, 16))
		$Vidas.add_child(vidas)
		GLOBAL.vidasMapa += 1
	if !GLOBAL.metaEsta && !GLOBAL.fin:
		var indise: int
		var meta = metaEsena.instantiate()
		indise = randi_range(1, 7)
		if indise == 1:
			meta.position = Vector3(randf_range(-100, 101), .65, randf_range(-100, 101))
		elif indise == 2:
			caminoAlto.progress_ratio = randf()
			meta.position = caminoAlto.global_position + Vector3(randf_range(-5, 6), 0, randf_range(-15, 16))
		elif indise == 3:
			meta.position = $"Cordenadas altas/Posicion 1".global_position
		elif indise == 4:
			meta.position = $"Cordenadas altas/Posicion 2".global_position
		elif  indise == 5:
			meta.position = $"Cordenadas altas/Posicion 3".global_position
		elif indise == 6:
			meta.position = $"Cordenadas altas/Posicion 4".global_position
		$Meta.add_child(meta)
		GLOBAL.metaEsta = true
	if GLOBAL.temp.is_stopped() && !GLOBAL.inicio:
		GLOBAL.fin = true
	if conosMax != 0:
		var cono = conoEsena.instantiate()
		cono.position = Vector3(randf_range(-200,201), 16, randf_range(-200,201))
		$"Obstaculos random".add_child(cono)
		conosMax -= 1
	if bloqueosMax != 0:
		var bloqueo = bloqueoEsena.instantiate()
		bloqueo.position = Vector3(randf_range(-200,201), 16, randf_range(-200,201))
		bloqueo.rotate_y(randf_range(0, 181))
		$"Obstaculos random".add_child(bloqueo)
		bloqueosMax -= 1

func _on_aparicion_meta_timeout():
	$"Aparicion meta/Sonido".play()
	GLOBAL.metaEsta = false

func _on_cuenta_de_inicio_timeout():
	$"Cuenta de inicio/Ya".play()
	GLOBAL.inicio = false
	GLOBAL.temp.start(tiempoMaxEnS)
	$"Aparicion meta".start(120)
