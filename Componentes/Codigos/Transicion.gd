extends CanvasLayer

func _ready():
	visible = false

func activar(esena: String) -> void:
	GLOBAL.nombreDeEsena = esena
	visible = true
	$Animacion.play("Transicion")
	await ($Animacion.animation_finished)
	get_window().content_scale_mode = 0
	get_tree().change_scene_to_file(esena)
	$Animacion.play_backwards("Transicion")
	await ($Animacion.animation_finished)
	visible = false
