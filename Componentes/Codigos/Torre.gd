extends MeshInstance3D

var objetivos: Array = []
var blanco: Area3D
@export var balaEsena: PackedScene

func _process(delta):
	if !objetivos.is_empty():
		if blanco == null:
			blanco = objetivos[0]
		else:
			$Cara.look_at(blanco.global_position)

func _on_rango_area_entered(area):
	if area.get_parent().name == "Jugador":
		objetivos.append(area)
		$Cara/Timer.start(1)

func _on_timer_timeout():
	if !GLOBAL.fin:
		var bala = balaEsena.instantiate()
		bala.position = $Cara/Marker3D.global_position
		bala.rotation = $Cara/Marker3D.global_rotation
		$Tiro.play()
		get_parent().add_child(bala)
		$Cara/Timer.start(1)

func _on_rango_area_exited(area):
	if area.get_parent().name == "Jugador":
		if area.name == blanco.name:
			blanco = null
		objetivos.erase(area)
		if objetivos.is_empty():
			blanco = null
			$Cara/Timer.stop()
