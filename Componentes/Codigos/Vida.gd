extends Area3D

var y: float
var cambio: bool

func _ready():
	y = global_position.y
	cambio = randi_range(0, 2)

func _process(delta):
	rotate_y(delta)
	if global_position.y >= y + 0.1:
		cambio = false
	elif global_position.y <= y -0.1:
		cambio = true
	if cambio:
		global_position.y += delta / 2
	else:
		global_position.y -= delta / 2

func _on_area_entered(area):
	if area.name != "Rango":
		GLOBAL.vidasMapa -= 1
		queue_free()

func _on_body_entered(body):
	if body.name != "Jugador":
		GLOBAL.vidasMapa -= 1
		queue_free()
