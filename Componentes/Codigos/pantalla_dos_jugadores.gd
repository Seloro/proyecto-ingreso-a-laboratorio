extends Control

@export var gana: Panel
@export var ganador: Label
@export var nombre: Label

func _ready():
	gana.visible = false

func _process(delta):
	if GLOBAL.fin:
		if !gana.visible:
			if GLOBAL.puntosJugadorUno > GLOBAL.puntosJugadorDos:
				nombre.text = "Jugador 1"
			elif  GLOBAL.puntosJugadorDos > GLOBAL.puntosJugadorUno:
				nombre.text = "Jugador 2"
			else:
				ganador.visible = false
				nombre.text = "Empate"
		gana.visible = true
	if GLOBAL.inicio:
		if GLOBAL.tempInicio <= 3:
			$Inico.text = str(int(GLOBAL.tempInicio) + 1)
	else:
		$Inico.visible = false

func _on_reintentar_pressed():
	$Aseptar.play()
	Transicion.activar(GLOBAL.nombreDeEsena)

func _on_volver_pressed():
	$Aseptar.play()
	Transicion.activar("res://Componentes/Esenas/menu_inicio.tscn")
