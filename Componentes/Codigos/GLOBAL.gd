class_name GLOBAL
extends Node

static var vidasMapa: int
static var cargadoresMapa: int
static var puntosJugadorUno: int
static var puntosJugadorDos: int
static var puntosJugadorTres: int
static var puntosJugadorCuatro: int
static var dosJugadores: bool
static var tresJugadores: bool
static var cuatroJugadores: bool
static var temp: Timer
static var metaEsta: bool
static var fin: bool
static var posicionUno: Vector3
static var rotacionUno: Vector3
static var posicionDos: Vector3
static var rotacionDos: Vector3
static var posicionTres: Vector3
static var rotacionTres: Vector3
static var posicionCuatro: Vector3
static var rotacionCuatro: Vector3
static var metaTemp: int
static var pausa: bool
static var inicio: bool
static var tempInicio: float
static var nombreDeEsena: String
static var direccionMetaEspecial: bool
static var progressRatioMeta: float
