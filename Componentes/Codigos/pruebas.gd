extends Node3D

@export var cargadorEsena: PackedScene
@export var camino: Path3D
@export var caminoSeguir: PathFollow3D
@export var cargadoresMax: int
@export var vidaEsena: PackedScene
@export var caminoVidas: Path3D
@export var caminoVidasSeguir: PathFollow3D
@export var vidasMax: int
@export var tiempoMaxEnS: int
@export var pantallaDosjugadores: PackedScene
@export var caminoMeta: Path3D
@export var caminoMetaSeguir: PathFollow3D
@export var metaEsena: PackedScene

func _ready():
	$Musica.play()
	if GLOBAL.dosJugadores:
		add_child(pantallaDosjugadores.instantiate())
	GLOBAL.cargadoresMapa = 0
	GLOBAL.vidasMapa = 0
	GLOBAL.temp = Timer.new()
	GLOBAL.temp.one_shot = true
	add_child(GLOBAL.temp)
	GLOBAL.temp.start(tiempoMaxEnS)
	GLOBAL.metaEsta = false
	GLOBAL.fin = false

func _process(delta):
	if GLOBAL.cargadoresMapa < cargadoresMax:
		caminoSeguir.progress_ratio = randf()
		var cargador = cargadorEsena.instantiate()
		cargador.position = caminoSeguir.global_position + (Vector3.RIGHT * randf_range(-10, 11)) + Vector3.FORWARD * randf_range(-10, 11)
		camino.add_child(cargador)
		GLOBAL.cargadoresMapa += 1
	if GLOBAL.vidasMapa < vidasMax:
		caminoVidasSeguir.progress_ratio = randf()
		var vidas = vidaEsena.instantiate()
		vidas.position = caminoVidasSeguir.global_position + (Vector3.RIGHT * randf_range(-10, 11)) + Vector3.FORWARD * randf_range(-10, 11)
		caminoVidas.add_child(vidas)
		GLOBAL.vidasMapa += 1
	if !GLOBAL.metaEsta && !GLOBAL.fin:
		caminoMetaSeguir.progress_ratio = randf()
		var meta = metaEsena.instantiate()
		meta.position = caminoMetaSeguir.global_position + (Vector3.RIGHT * randf_range(-10, 11)) + Vector3.FORWARD * randf_range(-10, 11)
		caminoMeta.add_child(meta)
		GLOBAL.metaEsta = true
	if GLOBAL.temp.is_stopped():
		GLOBAL.fin = true
