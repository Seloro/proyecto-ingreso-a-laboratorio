extends PathFollow3D

var velocidad: float

func _ready():
	velocidad = randf_range(8, 21)
	progress_ratio = GLOBAL.progressRatioMeta

func _process(delta):
	movimiento(delta)
	if GLOBAL.progressRatioMeta == 0:
		if progress_ratio == 1:
			GLOBAL.metaEsta = false
			queue_free()
	else:
		if progress_ratio == 0:
			GLOBAL.metaEsta = false
			queue_free()

func _on_meta_area_entered(area):
	if area.name != "Rango":
		if area.get_parent().name != "Jugador":  
			GLOBAL.metaEsta = false
		queue_free()

func movimiento(delta):
	if GLOBAL.direccionMetaEspecial:
		progress -= delta * velocidad
	else:
		progress += delta * velocidad
